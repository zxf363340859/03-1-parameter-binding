package com.twuc.webApp.jackson;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.bean.Person;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.io.Serializable;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
public class JacksonApiTest {

    private static ObjectMapper objectMapper = new ObjectMapper();


    @Test
    void test_integer_to_json() throws JsonProcessingException {
        Integer i = 1;
        String s = objectMapper.writeValueAsString(i);
        assertThat(s).isEqualTo("1");
    }

    @Test
    void test_json_to_integer() throws IOException {
        String s = "1";
        Integer integer = objectMapper.readValue(s, Integer.class);
        assertThat(integer).isEqualTo(1);
    }

    @Test
    void test_object_to_json() throws JsonProcessingException {
        Person person = new Person("zxf", "24");
        String s = objectMapper.writeValueAsString(person);
        assertThat(s).isEqualTo("{\"name\":\"zxf\",\"age\":\"24\"}");
    }

    @Test
    void test_json_to_object() throws IOException {
        String personStr = "{\"name\":\"zxf\",\"age\":\"24\"}";
        Person person = objectMapper.readValue(personStr, Person.class);
        assertThat(person.getAge()).isEqualTo("24");

    }
}
