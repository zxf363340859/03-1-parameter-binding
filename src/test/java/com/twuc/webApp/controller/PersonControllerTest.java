package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class PersonControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void test_not_null_annotation_success() throws Exception {
        mockMvc.perform(post("/person").content("{\"name\":\"zxf\",\"age\":\"24\"}").contentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE)).
                andExpect(status().is(200));
    }

    @Test
    void test_not_null_annotation_fail() throws Exception {
        mockMvc.perform(post("/person").content("{\"age\":\"24\"}").contentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE)).
                andExpect(status().is(400));
    }

    @Test
    void test_max_valid_annotation() throws Exception {
        mockMvc.perform(post("/person").content("{\"name\":\"zxf\",\"age\":\"24\", \"email\": \"sadasdasd\"}").contentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE)).
                andExpect(status().is(400));
    }

    @Test
    void test_email_valid_annotation() throws Exception {
        mockMvc.perform(post("/person").content("{\"name\":\"zxf\",\"age\":\"24\", \"email\": \"sadasdasd@qq.com\", \"addree\": \"aaaaaa\"}").contentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE)).
                andExpect(status().is(400));
    }

    @Test
    void test_request_body_should_return_person_object() throws Exception {
        mockMvc.perform(post("/person")
                .content("{\"name\":\"zxf\",\"age\":\"24\"}").contentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE))
                .andExpect(content().string("24"));
    }

    @Test
    void test_request_body_input_date_json() throws Exception {
        mockMvc.perform(post("/person/date")
                .content("{\"name\":\"zxf\",\"age\":\"24\", \"localDateTime\": \"2019-10-01T10:00:00Z\"}").contentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE))
                .andExpect(content().string("\"2019-10-01T10:00:00\""));
    }

    @Test
    void test_request_body_input_date_json_no() throws Exception {
        mockMvc.perform(post("/person/date")
                .content("{\"name\":\"zxf\",\"age\":\"24\", \"localDateTime\": \"2019-10-01\"}").contentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE))
                .andExpect(status().is4xxClientError());
    }
}
