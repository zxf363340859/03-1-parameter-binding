package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    // 2.1.1
    @Test
    void test_bind_var_to_integer() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/users/1")).andReturn().getResponse().getContentAsString();
        assertThat(contentAsString).isEqualTo("1");
    }


    //2.1.2
    @Test
    void test_bind_var_to_int() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/users/int/1").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andReturn().getResponse().getContentAsString();
        assertThat(contentAsString).isEqualTo("1");
    }

    //2.1.3
    @Test
    void test_bind_var_to_two_argu() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/users/3/books/3")).andReturn().getResponse().getContentAsString();
        assertThat(contentAsString).isEqualTo("3=3");
    }

    //2.2.1
    @Test
    void test_bind_var_to_one_argu_and_check_bind() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/users/part2").param("userId", "1")).andReturn().getResponse().getContentAsString();
        assertThat(contentAsString).isEqualTo("1");
    }

    //2.2.2
    @Test
    void test_bind_var_but_no_query_string() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/users/part2/second")).andReturn().getResponse().getContentAsString();
        assertThat(contentAsString).isEqualTo("5");
    }

    //2.2.3
    @Test
    void test_bind_var_with_collection() throws Exception {
        mockMvc.perform(get("/api/users/part2/collection?userId=1&userId=3")).andExpect(status().is2xxSuccessful());
    }


    @Test
    void test_request_param_annotation() throws Exception {
        mockMvc.perform(get("/api/users").param("clazz", "a").param("gender", "male")).
                andExpect(content().string("male"));
    }
}
