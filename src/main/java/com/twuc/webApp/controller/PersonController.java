package com.twuc.webApp.controller;

import com.twuc.webApp.bean.Person;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.logging.SimpleFormatter;

@RestController
public class PersonController {

    // 3 4
    @PostMapping("/person")
    public String getPerson(@RequestBody @Valid Person person) {
        return person.getAge();
    }


    @PostMapping("/person/date")
    public LocalDateTime testDate(@RequestBody @Valid Person person) {
        System.out.println(person.getLocalDateTime());
        return person.getLocalDateTime();
    }


}
