package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class UserController {

    // 2.1.1
    @GetMapping("/api/users/{id}")
    public String getUser(@PathVariable Integer id) {
        return id + "";
    }


    // 2.1.2  不能绑定到int 类型的变量
    @GetMapping("/api/users/int/{id}")
    public String getUserWithId(@PathVariable int id) {
        return id + "";
    }


    //2,1,3
    @GetMapping("/api/users/{userId}/books/{bookId}")
    public String getUser(@PathVariable Integer userId, @PathVariable Integer bookId) {
        return userId + "=" + bookId;
    }

    //2,2,1
    @GetMapping("/api/users/part2")
    public String getUser(@RequestParam String userId) {
        return userId + "";
    }

    //2,2,2  不提供变量 400
    @GetMapping("/api/users/part2/second")
    public String getUserWithDefaultValue(@RequestParam(defaultValue = "5") String userId) {
        return userId + "";
    }

    //2,2,2  不提供变量 400
    @GetMapping("/api/users/part2/collection")
    public String getUserWithCollection(ArrayList<String> userId) {
        return userId + "";
    }


    @GetMapping("/api/users")
    public String testUser(@RequestParam String clazz, @RequestParam String gender) {
        return gender;
    }

}
