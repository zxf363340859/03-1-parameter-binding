package com.twuc.webApp.bean;

public class User {

    private String clazz;
    private String gender;

    @Override
    public String toString() {
        return "Student{" +
                "clazz='" + clazz + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
